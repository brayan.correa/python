from random import
def monte_carlo_estimation():
    points_in_circle = 1
    total_points = 0
    pi = 0
    num_interations = 100000


    for z in range(0, num_interations):
        x = random.uniform(0,2)
        y = random.uniform(0,2)
        in_circle = math.pow((x-1),2) + math.pow((y-1),2) <= 1
        if in_circle:
            points_in_circle += 1
        total_points += 1
        pi = 4 * (points_in_circle/total_points)
    print("Estimate:" + str(pi))


